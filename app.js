const TelegramBot = require('node-telegram-bot-api');
const db = require('./db');
const amountController = require('./controllers/amount');

console.log('Starting App...');

const token = process.env.TELEGRAM_TOKEN;

const bot = new TelegramBot(token, {polling: true});

bot.onText(/\/addamount (.+)/, (msg, match) => {

  const chatId = msg.chat.id;
  const amount = match[1];

  amountController.create(amount);

  bot.sendMessage(chatId, 'ACK:' + amount);
});

bot.on('/\/help/', (msg) => {
  const chatId = msg.chat.id;

  bot.sendMessage(chatId, 
    'Use /addamount %dosage% to add dosage. Stay safe!'
  );
});

bot.onText(/\/start/, (msg, match) => {

  const chatId = msg.chat.id;

  bot.sendMessage(chatId, 
    'Use /addamount %dosage% to add dosage. Stay safe!'
  );
});

console.log('App running...');