const path = require('path');
const Amount = require('../models/amount');

exports.create = function (dosage) {
    var newAmount = new Amount({dosage: dosage});
    console.log('Amount received:' + dosage);
    newAmount.save(function (err) {
        if(err) {
            console.log('Unable to save amount to database');
            console.log(err);
        } else {
            console.log('Amount saved...');
        }
  });
};